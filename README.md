# mqtt2lgtv

[![](https://images.microbadger.com/badges/image/homesmarthome/mqtt2lgtv.svg)](https://microbadger.com/images/homesmarthome/mqtt2lgtv "mqtt2lgtv")

[![](https://images.microbadger.com/badges/version/homesmarthome/mqtt2lgtv.svg)](https://microbadger.com/images/homesmarthome/mqtt2lgtv "mqtt2lgtv")

Docker image providing a LG TV MQTT bridge.

## Run

```
docker run -d \
    --name mqtt2lgtv \
    --restart=unless-stopped \
    -v /config/env:/env \
    homesmarthome/mqtt2lgtv
```
Expects ```MQTT_HOST``` and ```LG_HOST``` value in env file.

Usage: https://www.npmjs.com/package/lgtv2mqtt