docker_run:
	docker run -d --name=mosquitto_test_run homesmarthome/mosquitto:latest
	docker run -d \
	  --name=mqtt2lgtv_test_run \
		--link mosquitto_test_run:mosquitto \
	  -v $(PWD)/test/env:/env \
	  $(DOCKER_IMAGE):$(DOCKER_TAG)
	docker ps | grep mqtt2lgtv_test_run

docker_stop:
	docker rm -f mqtt2lgtv_test_run 2> /dev/null ; true
	docker rm -f mosquitto_test_run 2> /dev/null; true
